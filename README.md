Jeg søker med dette på stillingen som frontend utvikler. 
Stillingsannonsen fant jeg på linkedin.com. 

Jeg er en siviløkonom som har et sterkt ønske om å jobbe med 
programmering eller digital markedsføring. Jeg har satt meg 
et 5 års mål om å lære meg godt både programmering og digital
markedsføring. 

Jeg har brukt 1,5 år på å lære meg det grunnleggende rundt 
programmering. Jeg driver nå å praktiserer det jeg har lært
gjennom å bygge applikasjoner fra start til slutt. Kursene 
jeg har tatt finner du på min linkedprofil: 
https://www.linkedin.com/in/dag-rune-strand-1989242/

I perioden 2011 til 2016 var jeg initiativtaker til å etablere
Smartify AS som utviklet en videobasert læringsportal. 
I de 5 årene jeg jobbet med Smartify AS ervervet jeg kunnskap 
om både Google Analytics, Adobe Photoshop, Google Adwords, 
Nyhetsbrev, Linkedin og Facebook. Læringsvidoene på portalen 
var redigert og produsert av meg med bruk av Adobe premier. 
Jeg har vedlagt et bilde av forsiden av portalen siden 
selskapet nå er avviklet. 

Etter sommeren startet jeg på fritiden læringsprogramet 
«AMA Professional certified marketer in Digital Marketing»
på lynda.com. Jeg ønsker å lære mest mulig om digital 
markedsføring. Dette i tillegg til å praktisere programmering.  

Som person er jeg veldig målrettet og har stor arbeidskapasitet. 
Jeg setter høye krav til meg selv og vil bli best i det jeg 
driver med.

Ønsker å jobbe i krysningen programmering og digital markedsføring. 

Jeg stiller gjerne til intervju.  

Med vennlig hilsen 
Dag Rune Strand
